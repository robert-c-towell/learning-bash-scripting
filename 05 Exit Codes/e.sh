#!/bin/bash

package=htop

sudo apt install $package >> /dev/null

if [ $? -eq 0 ]
then
    echo "The installation of $package was successful."
    echo "The new command is avaialble at:"
    which $package
else
    echo "$package failed to install." >> 
fi