#!/bin/bash

ls -l > /dev/null
echo "Running 'ls -l', exit code:" $?

ls -l /misc 2> /dev/null
echo "Running 'ls -l /misc', exit code:" $?