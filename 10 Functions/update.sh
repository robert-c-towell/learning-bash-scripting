#!/bin/bash

release_file=/etc/os-release
logfile=/var/log/updater.log
errorlog=/var/log/updater_errors.log

dist_name=$(grep -wA0 "NAME=" $release_file | sed 's/.*=//' | sed -e 's|"||g')

arch_dists=("Arch")
apt_dists=("Ubuntu" "Debian")

check_exit_status() {
  if [ $? -ne 0 ]; then
    echo "An error occurred, please check the $errorlog file."
  fi
}

if echo "${arch_dists[@]}" | grep -qw "$dist_name"; then
  # The host is based on Arch, run the pacman update command
  sudo pacman -Syu 1>>$logfile 2>>$errorlog
  check_exit_status
fi

if echo "${apt_dists[@]}" | grep -qw "$dist_name"; then
  # The host is based on Debian or Ubuntu,
  # Run the apt version of the command
  sudo apt update 1>>$logfile 2>>$errorlog
  check_exit_status

  sudo apt dist-upgrade -y 1>>$logfile 2>>$errorlog
  check_exit_status
fi
