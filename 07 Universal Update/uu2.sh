#!/bin/bash

release_file=/etc/os-release

dist_name=$(grep -wA0 "NAME=" $release_file | sed 's/.*=//' | sed -e 's|"||g')

arch_dists=("Arch")
apt_dists=("Ubuntu" "Debian")

if echo "${arch_dists[@]}" | grep -qw "$dist_name";
then
    # The host is based on Arch, run the pacman update command
    sudo pacman -Syu
fi

if echo "${apt_dists[@]}" | grep -qw "$dist_name";
then
    # The host is based on Debian or Ubuntu,
    # Run the apt version of the command
    sudo apt update
    sudo apt dist-upgrade
fi